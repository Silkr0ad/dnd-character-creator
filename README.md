# D&D Character Creator

<img src="Media/char_creation.png" width="400" />
<img src="Media/json_export.png" width="400" />


## General Information

10/5/2019 12:56 AM

**Author:** Demetre Saghliani, 821008260

**Proferssor:** Steve Price

**Assignment:** 2

## Credits

### Art

The only outside art assets used are icons, retrieved for free from flaticon.com.

Below is a list of all of them used in the project at any point in its development (that require citing, and whether or not they're currently sitting inside):

**sword:** Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/"             title="Flaticon">www.flaticon.com</a></div>

**foot:** Icons made by <a href="https://www.flaticon.com/authors/nhor-phai" title="Nhor Phai">Nhor Phai</a> from <a href="https://www.flaticon.com/"             title="Flaticon">www.flaticon.com</a></div>

**race:** Icons made by <a href="https://www.flaticon.com/authors/monkik" title="monkik">monkik</a> from <a href="https://www.flaticon.com/"             title="Flaticon">www.flaticon.com</a></div>

**class (bolt):** Icons made by <a href="https://www.flaticon.com/authors/iconnice" title="Iconnice">Iconnice</a> from <a href="https://www.flaticon.com/"             title="Flaticon">www.flaticon.com</a></div>

**alignment (fist):** Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/"             title="Flaticon">www.flaticon.com</a></div>

**xp (hourglass):** Icons made by <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons">Smashicons</a> from <a href="https://www.flaticon.com/"             title="Flaticon">www.flaticon.com</a></div>

**hp (heart):** Icons made by <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons">Smashicons</a> from <a href="https://www.flaticon.com/"             title="Flaticon">www.flaticon.com</a></div>

**armor class (shield):** Icons made by <a href="https://www.flaticon.com/authors/iconnice" title="Iconnice">Iconnice</a> from <a href="https://www.flaticon.com/"             title="Flaticon">www.flaticon.com</a></div>

**item list (pouch):** Icons made by <a href="https://www.flaticon.com/authors/surang" title="surang">surang</a> from <a href="https://www.flaticon.com/"             title="Flaticon">www.flaticon.com</a></div>

**die:** Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/"             title="Flaticon">www.flaticon.com</a></div>

**sparks:** Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/"             title="Flaticon">www.flaticon.com</a></div>

*Note: the above 2 were combined into the "Roll Dice" button.*

**shield NEW:** <div>Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/"             title="Flaticon">www.flaticon.com</a></div>

**innovation (level):** Icons made by <a href="https://www.flaticon.com/authors/geotatah" title="geotatah">geotatah</a> from <a href="https://www.flaticon.com/"             title="Flaticon">www.flaticon.com</a></div>

**[!!] The rest of the artwork is either made by self or imported from [Nucleo's free icon pack](https://nucleoapp.com/free-icons) or Google's Material icon pack, neither of which require citing.**


### Fonts

Garamond and [Hack](https://sourcefoundry.org/hack/) are used by the game. The former is installed by default on most machines and the latter is an open-source code font.

### Code

The following method for generating a random number inside a shader was taken from [here](https://gamedev.stackexchange.com/questions/32681/random-number-hlsl/32688#32688).

    frac(sin(dot(i.uv, float2(12.9898, 78.233) * 2.0)) * 43758.5453)

From [here](http://answers.unity.com/answers/345760/view.html) I copied the boilerplate for a transparent unlit shader.

From [here](https://thatfrenchgamedev.com/785/unity-2018-how-to-copy-string-to-clipboard/) I took the following function to get the "Copy to clipboard" button working.

    public void CopyOutput() {
        TextEditor te = new TextEditor();
        te.text = output.text;
        te.SelectAll();
        te.Copy();
    }

Many thanks to Justin Golden for helping me understand singletons and their intended use in this project.
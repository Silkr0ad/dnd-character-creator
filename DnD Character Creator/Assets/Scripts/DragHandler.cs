﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragHandler : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler {
    public Transform tempParent;
    public UI_Controller controller;
    public Transform homeParent;
    RectTransform t;
    Transform startParent;
    Text modifier;

    DragHandler ch;
    Transform boxHit;

    bool shouldReturn;
    bool useHomeTransform;
    bool replaceOnEnd;

    List<Vector2> transformData;
    List<string> abilityTypes;

    private void Start() {
        // initializing and caching
        abilityTypes = new List<string>();
        if (homeParent == null)
            homeParent = transform.parent;
        // ^ BUG: When returning from JSON scene, this will be set to its ability box if used,
        // so the functionality that depends on it won't work.
        // SOLUTION (?): Add homeParent's string in List<object> scoresInBox
        //   PROBLEM: requires a great deal of refactoring; fix later
        useHomeTransform = true;

        t = GetComponent<RectTransform>();

        #region Populating abilityTypes
        abilityTypes.Add("STR");
        abilityTypes.Add("DEX");
        abilityTypes.Add("CON");
        abilityTypes.Add("INT");
        abilityTypes.Add("WIS");
        abilityTypes.Add("CHA");
        #endregion
        #region Populating TransformData
        transformData = new List<Vector2>();

        transformData.Add(new Vector2(-27.5f, 0f));
        transformData.Add(new Vector2(1f, 0.5f));
        transformData.Add(new Vector2(1f, 0.5f));
        transformData.Add(new Vector2(0.5f, 0.5f));

        transformData.Add(new Vector2(0f, 0f));
        transformData.Add(new Vector2(0.5f, 0.5f));
        transformData.Add(new Vector2(0.5f, 0.5f));
        transformData.Add(new Vector2(0.5f, 0.5f));
        #endregion
    }

    public void OnBeginDrag(PointerEventData eventData) {
        startParent = transform.parent;
        shouldReturn = true;
        transform.SetParent(tempParent);
        // ^ this 3rd line is a work-around to the Mask component "Rolls",
        // which hides this object when it's dragged out of the panel
    }

    public void OnEndDrag(PointerEventData eventData) {
        if (shouldReturn) {
            SetPosition(startParent);
            if (startParent != homeParent)
                SetModifier();
        }
        if (replaceOnEnd) {
            ch.useHomeTransform = true;
            ch.SetPosition(ch.homeParent);

            shouldReturn = false;
            useHomeTransform = false;
            SetPosition(boxHit);


            SetModifier();
        }
    }

    // I'm not sure why, but Visual Studio required that I prepend "IDragHandler." to OnDrag,
    // and OnDrag only
    void IDragHandler.OnDrag(PointerEventData eventData) {
        transform.position = eventData.position;
        var results = controller.Query(eventData);

        foreach (var res in results) {
            string tag = res.gameObject.tag;

            // if we have hit an ability box...
            if (abilityTypes.Contains(tag) && res.gameObject.name == tag + " Box") {
                // if the ability box already contains a score...
                Transform child = res.gameObject.transform.Find("Score");
                if (child != null) {
                    replaceOnEnd = true;
                    ch = child.GetComponent<DragHandler>();
                    boxHit = res.gameObject.transform;
                    return;
                }
                
                shouldReturn = false;
                useHomeTransform = false;
                SetPosition(res.gameObject.transform);
                SetModifier();
            } else if (res.gameObject.name == "Dice Roller" && startParent != homeParent) {
                shouldReturn = false;
                useHomeTransform = true;
                SetPosition(homeParent);
            }  else {
                shouldReturn = true;
                replaceOnEnd = false;
                if (startParent == homeParent)
                    useHomeTransform = true;
                t.SetParent(tempParent);
                t.rotation = Quaternion.Euler(new Vector3(0f, 0f, -20f));

                if (modifier != null)
                    modifier.text = "";
            }
        }
    }

    private void SetModifier() {
        // this function assumes the score button is already a child of an ability box
        int scr = int.Parse(t.GetChild(0).GetComponent<Text>().text);
        modifier = t.parent.GetChild(0).GetChild(0).GetComponent<Text>();

        int m = (int)Mathf.Floor((scr - 10) / 2f);
        char sign;
        if (m < 0)
            sign = '-';
        else
            sign = '+';

        modifier.text = sign + Mathf.Abs(m).ToString();
    }

    void SetPosition(Transform parent) {
        t.SetParent(parent);

        int offset = 0;
        if (!useHomeTransform)
            offset = 4;

        t.pivot = transformData[3 + offset];
        t.anchorMin = transformData[1 + offset];
        t.anchorMax = transformData[2 + offset];
        t.anchoredPosition = transformData[0 + offset];

        t.rotation = Quaternion.Euler(new Vector3(0f, 0f, 0f));
    }
}
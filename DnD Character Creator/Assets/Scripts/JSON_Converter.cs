﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JSON_Converter : MonoBehaviour
{
    // Note: What's max XP and HP?
    public int maxXP;
    public int maxHP;

    public string GetJSON() {
        Dictionary<string, int> ai = new Dictionary<string, int>();

        #region Populating abilityIndices
        ai.Add("STR", 1);
        ai.Add("DEX", 2);
        ai.Add("CON", 3);
        ai.Add("INT", 4);
        ai.Add("WIS", 5);
        ai.Add("CHA", 6);
        #endregion

        Dictionary<string, string> lore = State.Instance.lore;
        List<string> attributeData = State.Instance.attributeData;
        List<object> scoresInBox = State.Instance.scoresInBox;

        string[] input = new string[20];

        #region Populating input
        // 1: fill Name
        if (attributeData[0] == "")
            input[0] = "null";
        else
            input[0] = attributeData[0];

        // 2: fill ability scores
        for (int i = 1; i < 7; i++) {
            input[i] = "null";
        }
        
        for (int i = 2; i < scoresInBox.Count; i += 4) {
            string grandparent = (string)scoresInBox[i];
            input[ai[grandparent]] = (string)scoresInBox[i - 1];
        }

        // 3: fill Race-Alignment
        int j = 6;
        for (int i = 7; i <= 11; i += 2) {
            if (i <= 10) {
                if (attributeData[i - j] == "Race" || attributeData[i - j] == "Class") {
                    input[i] = "null";
                    input[i + 1] = "null";
                    j++;
                    continue;
                }
                input[i] = attributeData[i - j];
                input[i + 1] = lore[attributeData[i - j]];
                j++;
            } else {
                if (attributeData[i - j] == "Alignment")
                    input[i] = "null";
                else
                    input[i] = attributeData[i - j];
            }
        }

        // 4: manually add XP and HP
        if (attributeData[5] == "")
            input[12] = "null";
        else
            input[12] = attributeData[5];
        input[13] = maxXP.ToString();
        if (attributeData[6] == "")
            input[14] = "null";
        else
            input[14] = attributeData[6];
        input[15] = maxHP.ToString();

        // 5. fill Armor Class and Speed
        for (int i = 16; i < 20; i++) {
            if (attributeData[i - 9] == "")
                input[i] = "null";
            else
                input[i] = attributeData[i - 9];
        }
        #endregion

        string output = @"{{
    ""Character Name"": ""{0}"",
    ""Abilities"": [
        {{ ""Ability_Strength"": {1} }},
        {{ ""Ability_Dexterity"": {2} }},
        {{ ""Ability_Constitution"": {3} }},
        {{ ""Ability_Intelligence"": {4} }},
        {{ ""Ability_Wisdom"": {5} }},
        {{ ""Ability_Charisma"": {6} }}
    ],
    ""Race"": ""{7} - {8}"",
    ""Class"": ""{9} - {10}"",
    ""Alignment"": ""{11}"",
    ""Experience Points"": [
        {{ ""Current"": {12} }},
        {{ ""Max"": {13} }}
    ],
    ""Hit Points"": [
        {{ ""Current"": {14} }},
        {{ ""Max"": {15} }}
    ],
    ""Armor Class"": {16},
    ""Speed"": [{17}, {18}, {19}],
    ""Item List"": []
}}";

        string formatted_output = string.Format(output, input);

        return formatted_output;
    }
}

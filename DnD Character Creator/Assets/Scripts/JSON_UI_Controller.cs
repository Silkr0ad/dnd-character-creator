﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JSON_UI_Controller : MonoBehaviour
{
    public InputField output;
    public Loader loader;
    public JSON_Converter jc;

    private void Start() {
        string output = jc.GetJSON();
        this.output.text = output;
    }

    public void CopyOutput() {
        TextEditor te = new TextEditor();
        te.text = output.text;
        te.SelectAll();
        te.Copy();
    }

    public void Back() {
        loader.Load("Creator");
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State : MonoBehaviour
{
    public static State Instance { get; private set; }

    public List<string> attributeData; // also used for JSON
    public Dictionary<string, Color> rollBreakdown;
    public List<string> scoresInRoll;
    public List<object> scoresInBox; // also used for JSON
    public List<string> modifiers;

    // for JSON:
    public Dictionary<string, string> lore;
    

    private void Awake() {
        if (Instance == null) {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        } else {
            Destroy(gameObject);
        }
    }
}

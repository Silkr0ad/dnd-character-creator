﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UI_Controller : MonoBehaviour {

    #region Class Members
    // references
    public Transform canvas;
    [Header("References For Info Display\n")]
    public Text infoTitle;
    public Text infoDescription;
    GraphicRaycaster gr;

    [Header("References For Saving State\n")]
    public Transform attributes;
    public Transform abilities;
    public Transform rollPanel;
    public Loader loader;

    // public fields
    [Header("Parameters\n")]
    public List<Transform> rolls;
    public Color normal;
    public Color inactive;

    // private fields
    Dictionary<string, string[]> infoDict;
    Dictionary<string, string> lore;
    string[] keys;
    #endregion

    private void Start() {
        // initializing fields and caching references
        gr = canvas.GetComponent<GraphicRaycaster>();

        infoDict = new Dictionary<string, string[]>();
        lore = new Dictionary<string, string>();

        // populating infoDict
        #region Populating infoDict and keys
        infoDict.Add("Name", new string[] { "Name", "Your character's name." });
        infoDict.Add("Race", new string[] { "Race", "Your character's race.\n\nOnce selected, hover over it for details." });
        infoDict.Add("Class", new string[] { "Class", "Your character's class.\n\nOnce selected, hover over it for details." });
        infoDict.Add("Alignment", new string[] { "Alignment", "Your character's moral alignment." });
        infoDict.Add("Level", new string[] { "Level & Experience", "Your character's experience. Starting characters usually have a level of 1 and XP of 0." });
        infoDict.Add("HP", new string[] { "HP", "Your character's hit points. When it goes down to 0, your character dies." });
        infoDict.Add("Armor Class", new string[] { "Armor Class", "Your character's armor class signifies how hard you are to hit. The greater it is, the greater the enemy's 1D20 roll must be to manage to hit you." });
        infoDict.Add("Speed", new string[] { "Walking & Running Speed, Jump Height", "Depending on your character's race, they have their own walking and running speed, as well as jumping height.\n\nMeasured in feet/second." });
        infoDict.Add("Item List", new string[] { "Item List", "A list of items in your character's possession. Incomplete." });
        infoDict.Add("Roll Toggle", new string[] { "Show Dice Simulator", "To fill your character's ability scores, you must roll dice. Click here to enable the dice simulator and make a roll." });
        infoDict.Add("Export", new string[] { "Export to JSON", "Click here to export your character's stats to a JSON format.\n\nOpens a new window." });
        infoDict.Add("Quit", new string[] { "Quit Game", "Click here to quit the game. Your data will be lost." });
        infoDict.Add("STR", new string[] { "Strength", "Your character's strength score, measuring physical power." });
        infoDict.Add("DEX", new string[] { "Dexterity", "Your character's dexterity score, measuring agility." });
        infoDict.Add("CON", new string[] { "Constitution", "Your character's constitution score, measuring endurance and physical toughness." });
        infoDict.Add("INT", new string[] { "Intelligence", "Your character's intelligence score, measuring reasoning and memory." });
        infoDict.Add("WIS", new string[] { "Wisdom", "Your character's wisdom score, measuring perception and insight." });
        infoDict.Add("CHA", new string[] { "Charisma", "Your character's charisma score measuring force of personality." });
        infoDict.Add("Dice Simulator", new string[] { "Dice Simulator", "In D&D, you rolls a 6-sided die 4 times, discard the smallest roll, then add the rest up. You can use that score for an ability of your choosing, such as DEX.\n\nThe number in [brackets] is the lowest number, so it is ignored in the summation.\n\nTo use a summed score, drag-and-drop it onto an ability score." });

        keys = infoDict.Keys.ToArray();
        #endregion
        #region Populating lore
        lore.Add("Dragonborn", "Your draconic heritage manifests in a variety of traits you share with other dragonborn.");
        lore.Add("Dwarf", "Your dwarf character has an assortment of abilities, part and parcel of dwarven nature.");
        lore.Add("Elf", "Your elf character has a variety of natural abilities, the result of thousands of years of elven refinement.");
        lore.Add("Gnome", "Your gnome character has certain characteristics in common with all other gnomes.");
        lore.Add("Half-Elf", "Your half-elf character has some qualities in common with elves and some that are unique to half-elves.");
        lore.Add("Half-Orc", "Your half-orc character has certain traits deriving from your orc ancestry.");
        lore.Add("Halfling", "Your halfling character has a number of traits in common with all other halflings.");
        lore.Add("Human", "It's hard to make generalizations about humans, but your human character has these traits.");
        lore.Add("Tiefling", "Tieflings share certain racial traits as a result of their infernal descent.");

        lore.Add("Barbarian", "In battle, you fight with primal ferocity. For some barbarians, rage is a means to an end–that end being violence.");
        lore.Add("Bard", "Whether singing folk ballads in taverns or elaborate compositions in royal courts, bards use their gifts to hold audiences spellbound.");
        lore.Add("Cleric", "Clerics act as conduits of divine power.");
        lore.Add("Druid", "Druids venerate the forces of nature themselves. Druids holds certain plants and animals to be sacred, and most druids are devote to one of the many nature deities.");
        lore.Add("Fighter", "Different fighters choose different approaches to perfecting their fighting prowess, but they all end up perfecting it.");
        lore.Add("Monk", "Coming from monasteries, monks are masters of martial arts combat and meditators with the ki living forces.");
        lore.Add("Paladin", "Paladins are the ideal of the knight in shining armor; they uphold ideals of justice, virtue, and order and use righteous might to meet their ends.");
        lore.Add("Ranger", "Acting as a bulwark between civilization and the terrors of the wilderness, rangers study, track, and hunt their favored enemies.");
        lore.Add("Rogue", "Rogues have many features in common, including their emphasis on perfecting their skills, their precise and deadly approach to combat, and their increasingly quick reflexes.");
        lore.Add("Sorcerer", "An event in your past, or in the life of a parent or ancestor, left an indelible mark on you, infusing you with arcane magic. As a sorcerer the power of your magic relies on your ability to project your will into the world.");
        lore.Add("Warlock", "You struck a bargain with an otherworldly being of your choice: the Archfey, the Fiend, or the Great Old One who has imbued you with mystical powers, granted you knowledge of occult lore, bestowed arcane research and magic on you and thus has given you facility with spells.");
        lore.Add("Wizard", "The study of wizardry is ancient, stretching back to the earliest mortal discoveries of magic. As a student of arcane magic, you have a spellbook containing spells that show glimmerings of your true power which is a catalyst for your mastery over certain spells.");
        #endregion

        // load data if needed
        if (State.Instance.attributeData.Count > 0) {
            Load();
        }
    }

    private void Update() {
        PointerEventData eventData = new PointerEventData(null);
        eventData.position = Input.mousePosition;
        DisplayInfo(Query(eventData));
    }

    private void DisplayInfo(List<RaycastResult> results) {
        foreach (var res in results) {
            if (keys.Contains(res.gameObject.tag)) {
                Dropdown dd = res.gameObject.GetComponent<Dropdown>();

                if (dd != null && (dd.gameObject.name == "Race" || dd.gameObject.name == "Class")) {
                    string val = dd.captionText.text;
                    if (val != "Race" && val != "Class") {
                        infoTitle.text = val;
                        infoDescription.text = lore[val];
                        continue;
                    }
                }

                string[] kv = infoDict[res.gameObject.tag];
                infoTitle.text = kv[0];
                infoDescription.text = kv[1];
            } 
        }
    }

    public List<RaycastResult> Query(PointerEventData eventData) {
        List<RaycastResult> results = new List<RaycastResult>();
        gr.Raycast(eventData, results);
        return results;
    }

    public void ToggleDiceRoller(bool isActive) {
        canvas.Find("Dice Roller").gameObject.SetActive(isActive);
    }

    public void Roll() {
        foreach (Transform roll in rolls) {
            // check if score has been assigned; if yes, gray it out and continue
            Text all = roll.GetChild(0).GetComponent<Text>();
            if (roll.childCount == 1) {
                all.color = inactive;
                continue;
            }
            all.color = normal;
            Text score = roll.GetChild(1).GetChild(0).GetComponent<Text>();

            roll.GetChild(1).gameObject.SetActive(true);
            
            // sum up and build string
            bool markedMin = false;
            int[] rs = new int[5];
            for (int i = 0; i < 5; i++) {
                rs[i] = (int)Mathf.Floor(UnityEngine.Random.Range(1f, 7f));
            }

            int sum = rs.Sum() - rs.Min();
            string allRolls = "";

            for (int i = 0; i < 5; i++) {
                string sign = i == 4 ? " =" : "+";
                if (rs[i] == rs.Min() && !markedMin) {
                    allRolls += "[" + rs[i].ToString() + "]" + sign;
                    markedMin = true;
                } else
                    allRolls += rs[i].ToString() + sign;
            }

            // assign the strings
            all.text = allRolls;
            score.text = sum.ToString();
        }
    }

    public void Quit() {
    #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
    #else
        Application.Quit();
    #endif
    }

    public void ExportToJSON() {
        Save();
        loader.Load("JSON_OUTPUT");
    }

    public void Save() {
        List<string> attributeData = new List<string>();
        Dictionary<string, Color> rollBreakdown = new Dictionary<string, Color>();
        List<string> scoresInRoll = new List<string>();
        List<object> scoresInBox = new List<object>();
        List<string> modifiers = new List<string>();

        #region Save Attributes
        for (int i = 0; i < attributes.childCount; i++) {
            InputField inField = attributes.GetChild(i).GetChild(0).GetComponent<InputField>();
            Dropdown dd = attributes.GetChild(i).GetChild(0).GetComponent<Dropdown>();

            if (dd != null) {
                attributeData.Add(dd.captionText.text);
            } else if (i == 4) {
                attributeData.Add(inField.text);
                attributeData.Add(inField.transform.parent.GetChild(1).GetComponent<InputField>().text);
            } else if (i == 7) {
                attributeData.Add(inField.text);
                attributeData.Add(inField.transform.parent.GetChild(1).GetComponent<InputField>().text);
                attributeData.Add(inField.transform.parent.GetChild(2).GetComponent<InputField>().text);
            } else {
                attributeData.Add(inField.text);
            }
        }
        #endregion
        #region Save Roll Breakdowns
        if (rollPanel.gameObject.activeInHierarchy == true) {
            for (int i = 0; i < 6; i++) {
                Text breakdown = rollPanel.GetChild(i).GetChild(0).GetComponent<Text>();
                rollBreakdown.Add(breakdown.text, breakdown.color);
            }
        }
        #endregion
        #region Save The Scores In Roller
        if (rollPanel.gameObject.activeInHierarchy == true) {
            for (int i = 0; i < 6; i++) {
                if (rollPanel.GetChild(i).childCount == 1) {
                    scoresInRoll.Add(null);
                } else {
                    scoresInRoll.Add(rollPanel.GetChild(i).GetChild(1).GetChild(0).GetComponent<Text>().text);
                }
            }
        }
        #endregion
        #region Save The Scores In Ability Boxes
        if (rollPanel.gameObject.activeInHierarchy == true) {
            for (int i = 0; i < 6; i++) {
                Transform score = abilities.GetChild(i).GetChild(1).Find("Score");
                if (score != null) {
                    RectTransform t = score.GetComponent<RectTransform>();
                    string number = score.GetChild(0).GetComponent<Text>().text;
                    Transform parent = score.parent;
                    Transform homeParent = score.gameObject.GetComponent<DragHandler>().homeParent;

                    // all this pain because non-primitive objects seem to get lost in a scene change:
                    List<Vector2> rect = new List<Vector2>();
                    rect.Add(t.position);
                    rect.Add(t.anchorMin);
                    rect.Add(t.anchorMax);
                    rect.Add(t.pivot);

                    string grandparentName = parent.parent.name;

                    scoresInBox.Add(rect);
                    scoresInBox.Add(number);
                    scoresInBox.Add(grandparentName);
                    scoresInBox.Add(homeParent.name);
                }
            }
        }
        #endregion
        #region Save Modifiers
        if (rollPanel.gameObject.activeInHierarchy == true) {
            for (int i = 0; i < 6; i++) {
                Text modifier = abilities.GetChild(i).GetChild(1).Find("Modifier Box").GetChild(0).GetComponent<Text>();
                modifiers.Add(modifier.text);
            }
        }
        #endregion

        State.Instance.attributeData = attributeData;
        State.Instance.rollBreakdown = rollBreakdown;
        State.Instance.scoresInRoll = scoresInRoll;
        State.Instance.scoresInBox = scoresInBox;
        State.Instance.modifiers = modifiers;
        State.Instance.lore = lore;
    }

    public void Load() {
        List<string> attributeData = new List<string>();
        Dictionary<string, Color> rollBreakdown = new Dictionary<string, Color>();
        List<string> scoresInRoll = new List<string>();
        List<object> scoresInBox = new List<object>();
        List<string> modifiers = new List<string>();

        attributeData = State.Instance.attributeData;
        rollBreakdown = State.Instance.rollBreakdown;
        scoresInRoll = State.Instance.scoresInRoll;
        scoresInBox = State.Instance.scoresInBox;
        modifiers = State.Instance.modifiers;

        #region Set Attributes
        int j = 0;
        for (int i = 0; i < attributes.childCount; i++) {
            InputField inField = attributes.GetChild(i).GetChild(0).GetComponent<InputField>();
            Dropdown dd = attributes.GetChild(i).GetChild(0).GetComponent<Dropdown>();

            if (dd != null) {
                dd.captionText.text = attributeData[i + j];
            } else if (i == 4) {
                inField.text = attributeData[i + j];
                inField.transform.parent.GetChild(1).GetComponent<InputField>().text = attributeData[i + j + 1];
                j++;
            } else if (i == 7) {
                inField.text = attributeData[i + j];
                inField.transform.parent.GetChild(1).GetComponent<InputField>().text = attributeData[i + j + 1];
                inField.transform.parent.GetChild(2).GetComponent<InputField>().text = attributeData[i + j + 2];
                j += 2;
            } else {
                inField.text = attributeData[i + j];
            }
        }
        #endregion
        #region Set Roll Breakdowns
        int k = 0;
        foreach (KeyValuePair<string, Color> entry in rollBreakdown) {
            Text breakdown = rollPanel.GetChild(k).GetChild(0).GetComponent<Text>();
            breakdown.text = entry.Key;
            breakdown.color = entry.Value;
            k++;
        }
        if (k > 0)
            ToggleDiceRoller(true);
        #endregion
        #region Set Scores
        j = 0;
        for (int i = 0; i < 6; i++) {
            Transform score = rollPanel.GetChild(i).Find("Score");

            score.gameObject.SetActive(true);

            string scoreNum = scoresInRoll[i];

            if (scoreNum != null) {
                score.GetChild(0).GetComponent<Text>().text = scoreNum;
            } else {
                string grandparent = (string)scoresInBox[j + 2];
                Transform parent = abilities.Find(grandparent).GetChild(1);
                score.SetParent(parent);

                RectTransform t = score.GetComponent<RectTransform>();
                List<Vector2> rectInfo = (List<Vector2>)scoresInBox[j];
                
                t.anchorMin = rectInfo[1];
                t.anchorMax = rectInfo[2];
                t.pivot = rectInfo[3];
                t.position = rectInfo[0];

                Text txt = score.GetChild(0).GetComponent<Text>();
                txt.text = (string)scoresInBox[j + 1];

                Transform homeParent = rollPanel.Find((string)scoresInBox[j + 3]);
                score.gameObject.GetComponent<DragHandler>().homeParent = homeParent;

                j += 4;
            } 
        }
        #endregion
        #region Set Modifiers
        for (int i = 0; i < 6; i++) {
            Text modifier = abilities.GetChild(i).GetChild(1).Find("Modifier Box").GetChild(0).GetComponent<Text>();
            modifier.text = modifiers[i];
        }
        #endregion
    }
}

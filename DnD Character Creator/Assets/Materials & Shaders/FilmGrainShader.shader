﻿Shader "Unlit/FilmGrainShader"
{
	// I know next to nothing about shaders, and less than that about how Unity implements them.
	// Much of this code is unused, and it could probably be more efficient, but this was the best I could do.
	// Was able to finally add transparency thanks to http://answers.unity.com/answers/345760/view.html
	// Noise generation is from https://gamedev.stackexchange.com/a/32688
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_Color("Color & Transparency", Color) = (0, 0, 0, 0.5)
    }
    SubShader
    {
        LOD 100
		Lighting Off
		ZWrite Off
		Cull Back
		Blend SrcAlpha OneMinusSrcAlpha
		Tags {"Queue" = "Transparent"}
		Color[_Color]

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			fixed4 _Color;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv) - _Color;
				float noise = (frac(sin(dot(i.uv, float2(12.9898, 78.233) * 2.0)) * 43758.5453));
				col.rgb += noise;
                return col;
            }
            ENDCG
        }
    }
}
